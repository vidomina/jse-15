package com.ushakova.tm.service;

import com.ushakova.tm.api.ICommandRepository;
import com.ushakova.tm.api.ICommandService;
import com.ushakova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
