package com.ushakova.tm.exception.system;

import com.ushakova.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("***Error: Argument Not Found***");
    }

}
