package com.ushakova.tm.exception.system;

import com.ushakova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("***Error: Command Not Found.***");
    }

}