package com.ushakova.tm.repository;

import com.ushakova.tm.api.ICommandRepository;
import com.ushakova.tm.constant.ArgumentConst;
import com.ushakova.tm.constant.TerminalConst;
import com.ushakova.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ARG_ABOUT, "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.ARG_VERSION, "Show application version."
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.ARG_HELP, "Show terminal commands."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close application."
    );

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.ARG_INFO, "Show system info."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, null, "Show program arguments."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Show task list."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Clear all tasks."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, null, "Show program commands."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Show project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Clear all projects."
    );
    // Task
    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, "Update task by index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, "Update task by id."
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConst.TASK_VIEW_BY_ID, null, "Show task by id."
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConst.TASK_VIEW_BY_NAME, null, "Show task by name."
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConst.TASK_VIEW_BY_INDEX, null, "Show task by index."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, "Remove task by id."
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null, "Remove task by name."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index."
    );

    private static final Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null, "Set \"In Progress\" status to task by id."
    );

    private static final Command TASK_START_BY_NAME = new Command(
            TerminalConst.TASK_START_BY_NAME, null, "Set \"In Progress\" status to task by name."
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null, "Set \"In Progress\" status to task by index."
    );

    private static final Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConst.TASK_COMPLETE_BY_ID, null, "Set \"Complete\" status to task by id."
    );

    private static final Command TASK_COMPLETE_BY_NAME = new Command(
            TerminalConst.TASK_COMPLETE_BY_NAME, null, "Set \"Complete\" status to task by name."
    );

    private static final Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConst.TASK_COMPLETE_BY_INDEX, null, "Set \"Complete\" status to task by index."
    );

    private static final Command TASK_CHANGE_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_BY_ID, null, "Change task status by id."
    );

    private static final Command TASK_CHANGE_BY_NAME = new Command(
            TerminalConst.TASK_CHANGE_BY_NAME, null, "Change task status by name."
    );

    private static final Command TASK_CHANGE_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_BY_INDEX, null, "Change task status by index."
    );

    // Project
    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, "Update project by id."
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConst.PROJECT_VIEW_BY_NAME, null, "Show project by name"
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConst.PROJECT_VIEW_BY_ID, null, "Show project by id."
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConst.PROJECT_VIEW_BY_INDEX, null, "Show project by index."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, "Remove project by id."
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null, "Remove project by name."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index."
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null, "Set \"In Progress\" status to project by id."
    );

    private static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConst.PROJECT_START_BY_NAME, null, "Set \"In Progress\" status to project by name."
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null, "Set \"In Progress\" status to project by index."
    );

    private static final Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_ID, null, "Set \"Complete\" status to project by id."
    );

    private static final Command PROJECT_COMPLETE_BY_NAME = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_NAME, null, "Set \"Complete\" status to project by name."
    );

    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_INDEX, null, "Set \"Complete\" status to project by index."
    );

    private static final Command PROJECT_CHANGE_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_BY_ID, null, "Change project status by id."
    );

    private static final Command PROJECT_CHANGE_BY_NAME = new Command(
            TerminalConst.PROJECT_CHANGE_BY_NAME, null, "Change project status by name."
    );

    private static final Command PROJECT_CHANGE_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_BY_INDEX, null, "Change project status by index."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, VERSION, HELP, INFO, ARGUMENTS, COMMANDS, TASK_CREATE, TASK_CLEAR,
            TASK_LIST, PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR, TASK_UPDATE_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_VIEW_BY_ID, TASK_VIEW_BY_NAME, TASK_VIEW_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_INDEX, TASK_START_BY_ID,
            TASK_START_BY_INDEX, TASK_START_BY_NAME, TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX,
            TASK_COMPLETE_BY_NAME, TASK_CHANGE_BY_NAME, TASK_CHANGE_BY_ID, TASK_CHANGE_BY_INDEX,
            PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID, PROJECT_VIEW_BY_NAME, PROJECT_VIEW_BY_ID,
            PROJECT_VIEW_BY_INDEX, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_START_BY_NAME, PROJECT_COMPLETE_BY_ID,
            PROJECT_COMPLETE_BY_INDEX, PROJECT_COMPLETE_BY_NAME, PROJECT_CHANGE_BY_NAME,
            PROJECT_CHANGE_BY_ID, PROJECT_CHANGE_BY_INDEX, EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}