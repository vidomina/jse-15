package com.ushakova.tm.repository;

import com.ushakova.tm.api.IProjectRepository;
import com.ushakova.tm.exception.empty.EmptyNameException;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projectList = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return projectList;
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(projectList);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public void add(final Project project) {
        projectList.add(project);
    }

    @Override
    public void remove(final Project project) {
        projectList.remove(project);
    }

    @Override
    public void clear() {
        projectList.clear();
    }

    @Override
    public Project removeOneByName(String name) {
        final Project project = findOneByName(name);
        if (name == null) throw new EmptyNameException();
        remove(project);
        return project;
    }

    @Override
    public Project removeOneById(final String id) {
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projectList) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projectList.get(index);
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project : projectList) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

}
