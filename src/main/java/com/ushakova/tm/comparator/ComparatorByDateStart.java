package com.ushakova.tm.comparator;

import com.ushakova.tm.api.entity.IHasDateStart;

import java.util.Comparator;

public final class ComparatorByDateStart implements Comparator<IHasDateStart> {

    private final static ComparatorByDateStart INSTANCE = new ComparatorByDateStart();

    public static ComparatorByDateStart getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(IHasDateStart o1, IHasDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
