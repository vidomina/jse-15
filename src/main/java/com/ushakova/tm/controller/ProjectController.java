package com.ushakova.tm.controller;

import com.ushakova.tm.api.IProjectController;
import com.ushakova.tm.api.IProjectService;
import com.ushakova.tm.enumerated.Sort;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        System.out.println("***Project List***");
        System.out.println("***Enter Sort: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects = new ArrayList<>();
        if (sort == null || sort.isEmpty()) projects = projectService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("***Project Create***");
        System.out.println("Enter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void clear() {
        System.out.println("*Project Clear*");
        projectService.clear();
    }

    @Override
    public void removeOneByName() {
        System.out.println("***Remove Project***\nEnter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void removeOneById() {
        System.out.println("***Remove Project***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void removeOneByIndex() {
        System.out.println("***Remove Project***\nEnter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void findOneById() {
        System.out.println("***Show Project***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProjectInfo(project);
    }

    @Override
    public void findOneByIndex() {
        System.out.println("***Show Project***\nEnter Index:\"");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        showProjectInfo(project);
    }

    @Override
    public void findOneByName() {
        System.out.println("***Show Project***\nEnter Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        showProjectInfo(project);
    }

    @Override
    public void updateProjectById() {
        System.out.println("***Update Project***\nEnter Id:\"");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(id, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("***Update Project***\nEnter Index:\"");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectById() {
        System.out.println("***Set Status \"In Progress\" to Project***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("***Set Status \"In Progress\" to Project***\nEnter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectByName() {
        System.out.println("***Set Status \"In Progress\" to Project***\nEnter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void completeProjectById() {
        System.out.println("***Set Status \"Complete\" to Project***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.completeProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("***Set Status \"Complete\" to Project***\nEnter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.completeProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void completeProjectByName() {
        System.out.println("***Set Status \"Complete\" to Project***\nEnter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.completeProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeStatusByName() {
        System.out.println("Enter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeStatusByName(name, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeStatusById(id, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeStatusByIndex(index, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    private void showProjectInfo(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        ;
        System.out.println("Id: " + project.getId() + "\nName: " + project.getName()
                + "\nDescription: " + project.getDescription()
                + "\nStatus: " + project.getStatus().getDisplayName());
    }

}
