package com.ushakova.tm.api;

import com.ushakova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task removeOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

}