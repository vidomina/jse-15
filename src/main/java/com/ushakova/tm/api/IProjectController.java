package com.ushakova.tm.api;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void removeOneByName();

    void removeOneById();

    void removeOneByIndex();

    void findOneById();

    void findOneByIndex();

    void findOneByName();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void startProjectByName();

    void completeProjectById();

    void completeProjectByIndex();

    void completeProjectByName();

    void changeStatusByName();

    void changeStatusById();

    void changeStatusByIndex();

}
