package com.ushakova.tm.api;

import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task add(String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task removeOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task completeTaskById(String id);

    Task completeTaskByIndex(Integer index);

    Task completeTaskByName(String name);

    Task changeStatusByName(String name, Status status);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

}
